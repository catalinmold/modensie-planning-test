import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Assignment, AssignmentCourse, Course, Meeting, ParticipantCourse,
  UserAccount, UserAccountEmailAddress, UserAccountPassword
} from './data.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataRequestService {

   constructor(private http: HttpClient) {
  }

  // --------------- User Account
  // GET
  public getUserAccounts(): Observable<UserAccount[]> {
    return this.http.get<UserAccount[]>('//localhost:8080/userAccounts/allUsers');
  }

  public getUserAccountDetails(username: string): Observable<UserAccount> {
    return this.http.get<UserAccount>('//localhost:8080/userAccounts/AccountDetailsOfAnUsername?username=' + username);
  }

  // INSERT
  public addUserAccount(user) {
    return this.http.post<UserAccount>('//localhost:8080/userAccounts/post/insertUser', user);
  }

  // PUT
  public editUserAccountEmailAddress(userEmailAddress) {
    return this.http.put<UserAccountEmailAddress>('//localhost:8080/userAccounts/put/editAccountEmailAddress', userEmailAddress);
  }

  public editUserAccountPassword(userPassword) {
    return this.http.put<UserAccountPassword>('//localhost:8080/userAccounts/put/editAccountPassword', userPassword);
  }

  // DELETE
  public deleteUserAccount(username) {
    return this.http.request('delete', '//localhost:8080/userAccounts/delete/deleteUser?username=' + username);
  }

  // --------------- Course
  // GET
  public getAllCourses(): Observable<Course[]> {
    return this.http.get<Course[]>('//localhost:8080/courses/allCourses');
  }

  public getAllCoursesName(): Observable<string[]> {
    return this.http.get<string[]>('//localhost:8080/courses/allCoursesName');
  }

  public getAllCoursesNameOfUser(username: string): Observable<string[]> {
    return this.http.get<string[]>('//localhost:8080/participantCourse/allCoursesOfUser?username=' + username);
  }

  public getAllCoursesNameNotFollowedByUser(username: string): Observable<string[]> {
    return this.http.get<string[]>('//localhost:8080/participantCourse/allCoursesNotFollowedByUser?username=' + username);
  }

  public checkCourse(courseName: string): Observable<Course> {
    return this.http.get<Course>('//localhost:8080/courses/checkCourse?courseName=' + courseName);
  }

  public addCourse(course) {
    return this.http.post<Course>('//localhost:8080/courses/post/insertCourse', course);
  }

  // DELETE
  public deleteCourse(courseName) {
    return this.http.request('delete', '//localhost:8080/courses/delete/deleteCourse?courseName=' + courseName);
  }

  // --------------- Participant Course
  // INSERT
  public addParticipantToCourse(assignation) {
    return this.http.post<ParticipantCourse>('//localhost:8080/participantCourse/post/insertParticipantCourse', assignation);
  }

  // DELETE
  public deleteParticipantCourse(username: string, courseName: string) {
    return this.http.request('delete', '//localhost:8080/participantCourse/delete/deleteParticipantCourse?username=' + username + '&courseName=' + courseName);
  }

  // --------------- Assignment
  // GET
  public getAllAssignmentsOfCourse(courseName: string): Observable<Assignment[]> {
    return this.http.get<Assignment[]>('//localhost:8080/assignment/allAssignmentsOfCourse?courseName=' + courseName);
  }

  // INSERT
  public addAssignment(assignment) {
    return this.http.post<Assignment>('//localhost:8080/assignment/post/insertAssignment', assignment);
  }

  // PUT
  public editAssignment(assignmentCourse) {
    return this.http.put<AssignmentCourse>('//localhost:8080/assignment/put/editAssignment', assignmentCourse);
  }

  // DELETE
  public deleteAssignment(assignmentName: string, courseName: string) {
    return this.http.request('delete', '//localhost:8080/assignment/delete/deleteAssignmentCourse?assignmentName=' + assignmentName + '&courseName=' + courseName);
  }

  // --------------- Meeting
  // GET
  public getAllMeetingsOfUser(username: string): Observable<Meeting[]> {
    return this.http.get<Meeting[]>('//localhost:8080/meeting/allMeetingsOfUser?username=' + username);
  }
}
