import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DataRequestService} from '../core/data.service';
import {CookieService} from 'ngx-cookie-service';
import {Assignment, Course, Meeting, ParticipantCourse} from '../core/data.model';

@Component({
  selector: 'app-patient-page',
  templateUrl: './student-page.component.html',
  styleUrls: ['./student-page.component.css']
})
export class StudentPageComponent implements OnInit {
  userCoursesNotFollowed: string[];
  userCourses: string[];
  assignmentList: Array<Assignment>;

  meetingList: Array<Meeting>;
  public timelineChartData: any =  {
    chartType: 'Timeline',
    options: {
      height: 400
    },
    dataTable: [
      [{ type: 'string', id: 'Term' }, { type: 'string', id: 'Name' }, { type: 'string', role: 'style' }, { type: 'date', id: 'Start' }, { type: 'date', id: 'End' }]
    ]
  };
  public firstDayOfWeek: Date;
  public lastDayOfWeek: Date;

  courseNameSelected: string;
  coursePassword: string;

  private checkCourse: Course;
  private newAssignation: ParticipantCourse;
  private day: string;
  constructor(private router: Router, private dataRequestService: DataRequestService, private cookieService: CookieService) { }


  ngOnInit(): void {
    if (!this.cookieService.check('userLogged')) {
      this.router.navigate(['login_page']);
    } else
    if (this.cookieService.get('userRole') !== 'student' ) {
      this.router.navigate(['error_page']);
    }

    this.dataRequestService.getAllMeetingsOfUser(this.cookieService.get('userLogged'))
      .subscribe(data => {
        this.meetingList = data;

        const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        this.firstDayOfWeek = new Date(new Date().setDate(new Date().getDate() - new Date().getDay() + 1));
        this.lastDayOfWeek = new Date(new Date().setDate(new Date().getDate() - new Date().getDay() + 7));

        // Add Empty fields
        this.timelineChartData.dataTable.push(['Monday', '', 'opacity: 0', new Date().setHours(8, 0, 0, 0), new Date().setHours(8, 0, 0, 0)]);
        for (const day of days) {
          this.timelineChartData.dataTable.push([day, '', 'opacity: 0', new Date().setHours(22, 0, 0, 0), new Date().setHours(22, 0, 0, 0)]);
        }

        // Add Data
        let isEmpty = true;
        for (const meeting of this.meetingList) {
          if (new Date(meeting.start_time) >= this.firstDayOfWeek && new Date(meeting.start_time) <= this.lastDayOfWeek) {
            isEmpty = false;
            // if (new Date(meeting.start_time) > new Date())
            this.timelineChartData.dataTable.push([days[new Date(meeting.start_time).getDay()], meeting.courseName + ' (' + meeting.name + ')', null, new Date(meeting.start_time).setDate(new Date().getDate()), new Date(meeting.end_time).setDate(new Date().getDate())]);
          }
        }
        if (!isEmpty) {
          (document.querySelector('.chart-timetable') as HTMLElement).style.display = 'block';
        }
      });


    this.dataRequestService.getAllCoursesNameOfUser(this.cookieService.get('userLogged'))
      .subscribe(data => {
        this.userCourses = data;
      });

    this.dataRequestService.getAllCoursesNameNotFollowedByUser(this.cookieService.get('userLogged'))
      .subscribe(data => {
        this.userCoursesNotFollowed = data;
      });
  }

  showAssignment(courseName: string): void {
    (document.querySelector('.table-student-assignment') as HTMLElement).style.display = 'block';
    this.dataRequestService.getAllAssignmentsOfCourse(courseName)
      .subscribe(data => {
        this.assignmentList = data;

        for (let i = 0; i < this.assignmentList.length; i++) {
          this.assignmentList[i].date = new Date(this.assignmentList[i].date).toDateString();
          switch (Number(this.assignmentList[i].type)){
            case 0:
              this.assignmentList[i].type = 'lesson';
              break;
            case 1:
              this.assignmentList[i].type = 'exercise';
              break;
            default:
              this.assignmentList[i].type = 'news';
              break;
          }
        }
      });
  }

  showPasswordBox(courseName: string): void {
    (document.querySelector('.table-password-box') as HTMLElement).style.display = 'block';
    this.courseNameSelected = courseName;
  }

  joinCourse(courseName: string): void {
      this.dataRequestService.checkCourse(courseName)
        .subscribe(data => {
          this.checkCourse = data;

          if (this.checkCourse) {
            if (this.coursePassword === this.checkCourse.password || (this.coursePassword === undefined && !this.checkCourse.password)) {
              // Assign User to Course
              this.newAssignation = new ParticipantCourse();
              this.newAssignation.courseName = courseName;
              this.newAssignation.usernameUser = this.cookieService.get('userLogged');

              this.dataRequestService.addParticipantToCourse(this.newAssignation)
                .subscribe(participantData => location.reload());
            }
          }
        });
  }

  leaveCourse(courseName: string): void {
    this.dataRequestService.deleteParticipantCourse(this.cookieService.get('userLogged'), courseName)
      .subscribe( data => location.reload());
  }

  logout(): void {
    this.cookieService.delete('userLogged');
    this.cookieService.delete('userRole');
  }

  onSelect(event): void{
    const { row, column } = event[0];
    let selectedItem;
    if (column === 1) {
      selectedItem = 'current';
    }
    if (column === 2) {
      selectedItem = 'target';
    }
    console.log('SelectedItem' + selectedItem);
  }
}
