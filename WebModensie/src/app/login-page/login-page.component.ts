import { Component, OnInit } from '@angular/core';
import { DataRequestService} from '../core/data.service';
import {UserAccount, UserAccountPassword} from '../core/data.model';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  users: Array<UserAccount>;
  userAccount: UserAccountPassword = new UserAccountPassword();
  errorMessage: string;
  newUserAccount: UserAccount = new UserAccount();



  constructor(private router: Router, private dataRequestService: DataRequestService, private cookieService: CookieService) { }
  // Router is used for redirecting to other page .navigate
  // UserService is used to get functions from it .dataRequestService

  ngOnInit() { // Get data
    if (this.cookieService.check('userLogged')) {
      this.router.navigate(['main_page']);
    }

    this.dataRequestService.getUserAccounts().subscribe(data => {
      this.users = data;
    });
  }

  public login(): void {
    this.dataRequestService.getUserAccounts().subscribe(data => {
      this.users = data;
    });

    let validLogin = false;
    for (const userDetails of this.users) {
      if (this.userAccount.username === userDetails.username) {
        validLogin = true;
        if (this.userAccount.password === userDetails.password) {
          const loginExpire = new Date();
          loginExpire.setMinutes(loginExpire.getMinutes() + 10); // 10 minutes
          this.cookieService.set('userLogged', userDetails.username, loginExpire, '/'); // Store username in a cookie
          this.cookieService.set('userRole', userDetails.role, loginExpire, '/'); // Store username in a cookie
          this.router.navigate(['main_page']);
        } else {
          this.errorMessage = 'Invalid password';
        }
      }
    }
    if (!validLogin) {
      this.errorMessage = 'Invalid credentials';
    }
  }

  public register(): void {
    if (this.newUserAccount.username !== undefined && this.newUserAccount.password !== undefined && this.newUserAccount.name !== undefined && this.newUserAccount.emailAddress !== undefined)
    {
      const gender = (document.getElementById('SelectGender')) as HTMLSelectElement;
      const role = (document.getElementById('SelectRole')) as HTMLSelectElement;
      this.newUserAccount.gender = gender.options[gender.selectedIndex].value;
      this.newUserAccount.role = role.options[role.selectedIndex].value;

      this.newUserAccount.birthdate = formatDate(this.newUserAccount.birthdate, 'dd-MM-YYYY', 'en_US');

      this.dataRequestService.addUserAccount(this.newUserAccount)
        .subscribe(
          data => location.reload());
    }
    else {
      this.errorMessage = 'Invalid credentials';
    }
  }

  public showField(fieldName: string): void {
    if (fieldName === 'loginField') {
      if ((document.querySelector('.login-form') as HTMLElement).style.display === 'block')
      {
        this.login();
      }
      else {
        this.errorMessage = '';
        (document.querySelector('.login-form') as HTMLElement).style.display = 'block';
        (document.querySelector('.card-register') as HTMLElement).style.display = 'none';
        (document.querySelector('.card-back') as HTMLElement).style.display = 'block';
      }
    }
    else if (fieldName === 'registerField') {
      if ((document.querySelector('.register-form') as HTMLElement).style.display === 'block')
      {
        this.register();
      }
      else {
        this.errorMessage = '';
        (document.querySelector('.register-form') as HTMLElement).style.display = 'block';
        (document.querySelector('.card-login') as HTMLElement).style.display = 'none';
        (document.querySelector('.card-back') as HTMLElement).style.display = 'block';
      }
    }
    else if (fieldName === 'backField') {
      (document.querySelector('.login-form') as HTMLElement).style.display = 'none';
      (document.querySelector('.register-form') as HTMLElement).style.display = 'none';
      (document.querySelector('.card-back') as HTMLElement).style.display = 'none';
      (document.querySelector('.card-register') as HTMLElement).style.display = 'block';
      (document.querySelector('.card-login') as HTMLElement).style.display = 'block';
    }
  }


}
