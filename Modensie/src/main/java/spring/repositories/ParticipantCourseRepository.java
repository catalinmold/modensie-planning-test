package spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.entities.ParticipantCourse;
import spring.entities.UserAccount;

import java.util.List;

@Repository
public interface ParticipantCourseRepository extends JpaRepository<ParticipantCourse,Integer> {

    @Query(value="SELECT Course.name FROM Course JOIN Participant_Course ON Course.course_id = Participant_Course.course_id " +
            "JOIN User_Account ON User_Account.user_account_id = Participant_Course.user_id " +
            "WHERE User_Account.username = (?1)", nativeQuery = true)
    public List<String> findAllCoursesOfUser(String username);

    @Query(value="SELECT Course.name FROM Course WHERE course_id NOT IN ( " +
            "SELECT Course.course_id FROM Course JOIN Participant_Course ON Course.course_id = Participant_Course.course_id " +
            "JOIN User_Account ON User_Account.user_account_id = Participant_Course.user_id " +
            "WHERE User_Account.username = (?1))", nativeQuery = true)
    public List<String> findAllCoursesNotFollowedByUser(String username);

    @Query(value="SELECT User_Account.* FROM User_Account JOIN Participant_Course ON user_account_id = user_id " +
            "JOIN Course ON Course.course_id = Participant_Course.course_id " +
            "WHERE Course.name = (?1)", nativeQuery = true)
    public List<Object[]> findAllParticipantsOfCourse(String courseName);

    @Query(value="SELECT username FROM User_Account JOIN Participant_Course ON user_account_id = user_id " +
            "JOIN Course ON Course.course_id = Participant_Course.course_id " +
            "WHERE User_Account.role = 'student' AND Course.name = (?1)", nativeQuery = true)
    public List<String> findAllStudentsOfCourse(String courseName);

    @Query(value="SELECT username FROM User_Account JOIN Participant_Course ON user_account_id = user_id " +
            "JOIN Course ON Course.course_id = Participant_Course.course_id " +
            "WHERE User_Account.role ='teacher' AND Course.name = (?1)", nativeQuery = true)
    public List<String> findAllTeachersOfCourse(String courseName);

    @Query(value="SELECT Participant_Course.* FROM Course JOIN Participant_Course ON Course.course_id = Participant_Course.course_id " +
            "JOIN User_Account ON User_Account.user_account_id = Participant_Course.user_id " +
            "WHERE User_Account.username = (?1) AND Course.name = (?2)", nativeQuery = true)
    public ParticipantCourse checkUserAssignedToCourse(String username, String courseName);
}
