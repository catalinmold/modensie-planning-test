package spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.entities.Course;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course,Integer> {

    @Query(value="SELECT * FROM Course WHERE name = (?1)", nativeQuery = true)
    public Course findByCourseName(String courseName);

    @Query(value="SELECT name FROM Course", nativeQuery = true)
    public List<String> getAllCourses();
}
