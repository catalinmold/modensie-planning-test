package spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import spring.entities.UserAccount;

import java.util.List;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount,Integer> {

    @Query(value="SELECT * FROM User_Account WHERE username = (?1)", nativeQuery = true)
    public UserAccount findByUsername(String userUsername);

    @Query(value="SELECT username FROM User_Account WHERE role = 'teacher';", nativeQuery = true)
    public List<String> findAllTeachers();

    @Query(value="SELECT username FROM User_Account WHERE role = 'student';", nativeQuery = true)
    public List<String> findAllStudents();
}
