package spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.dto.AssignmentCourseDTO;
import spring.dto.AssignmentDTO;
import spring.entities.Assignment;
import spring.services.AssignmentService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200") // CORS policy: No 'Access-Control-Allow-Origin'
@RestController
@RequestMapping("/assignment") // http://localhost:8080(/assignment)
public class AssignmentController {

    @Autowired
    AssignmentService assignmentService;

    //@GetMapping
    //@PostMapping
    //@PutMapping
    //@DeleteMapping

    //---------------- (GET Requests)

    // Find All Assignments of Course
    @GetMapping(value={"allAssignmentsOfCourse"})// (/assignment)/allAssignmentsOfCourse
    private List<AssignmentDTO> getAllAssignments(@RequestParam("courseName") String courseName){
        return assignmentService.findAllAssigmentOfCourse(courseName);
    }

    // CHECK/GET Assignment Assigned to Course
    @GetMapping(value={"checkAssignmentAssignedToCourse"})
    private Assignment checkAssignmentAssignedToCourse(@RequestParam("assignmentName") String assignmentName, @RequestParam("courseName") String courseName){
        return assignmentService.checkAssignmentAssignedToCourse(assignmentName, courseName);
    }

    //---------------- (POST Requests)

    // INSERT Assignment
    @PostMapping(value = { "/post/insertAssignment" })
    private void addAssignment(@RequestBody AssignmentDTO assignment) {
        assignmentService.addAssignment(assignment);
    }

    // UPDATE Assignment
    @PutMapping (value = { "/put/editAssignment" })
    private void editAssignment (@RequestBody AssignmentCourseDTO assignmentCourse) {
        assignmentService.editAssignment(assignmentCourse);
    }

    //---------------- (DELETE Requests)

    // DELETE Assignment
    @DeleteMapping(value = { "/delete/deleteAssignmentCourse" })
    private void deleteParticipantCourse(@RequestParam("assignmentName") String assignmentName, @RequestParam("courseName") String courseName) {
        assignmentService.deleteAssignment(assignmentName, courseName);
    }
}
