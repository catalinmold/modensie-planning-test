package spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.dto.UserEmailAddressDTO;
import spring.dto.UserLoginDTO;
import spring.entities.UserAccount;
import spring.services.UserAccountService;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:4200") // CORS policy: No 'Access-Control-Allow-Origin'
@RestController
@RequestMapping("/userAccounts") // http://localhost:8080(/userAccounts)
public class UserAccountController {

    @Autowired
    UserAccountService userService;

    //@GetMapping
    //@PostMapping
    //@PutMapping
    //@DeleteMapping

    //---------------- (GET Requests)

    // Find All Users
    @GetMapping(value={"allUsers"})// (/userAccounts)/allUsers
    private List<UserAccount> getAllUser(){
        return userService.findAllUserAccount();
    }

    // GET All Teachers
    @GetMapping(value={"allTeachers"})
    private List<String> getAllCaregivers(){
        return userService.findAllTeachers();
    }

    // GET All Students
    @GetMapping(value={"allStudents"})
    private List<String> getAllStudents(){
        return userService.findAllStudents();
    }

    // GET Account Details
    @GetMapping(value={"AccountDetailsOfAnUsername"})
    private UserAccount getAccountDetailsFromUsername(@RequestParam("username") String username){
        return userService.findAccountDetailsFromUsername(username);
    }

    // GET Account Id
    @GetMapping(value={"AccountId"})
    private UUID getAccountId(@RequestParam("username") String username){
        return userService.findAccountId(username);
    }

    //---------------- (PUT Requests)

    // UPDATE Account Email Address
    @PutMapping (value = { "/put/editAccountEmailAddress" })
    private void editUserAddress (@RequestBody UserEmailAddressDTO username) { userService.editUserEmailAddress(username);
    }

    // UPDATE Account Password
    @PutMapping (value = { "/put/editAccountPassword" })
    private void editUserPassword (@RequestBody UserLoginDTO username) {
        userService.editUserPassword(username);
    }

    //---------------- (POST Requests)

    // INSERT User (CRUD operations on users)
    @PostMapping(value = { "/post/insertUser" })
    private void registerUser(@RequestBody UserAccount userAccount) {
        userService.registerUser(userAccount);
    }

    //---------------- (DELETE Requests)

    // DELETE User
    @DeleteMapping(value = { "/delete/deleteUser" })
    private void deleteUser(@RequestParam("username") String username) {
        userService.deleteUser(username);
    }
}
