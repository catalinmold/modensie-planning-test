package spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.dto.AssignmentDTO;
import spring.dto.ParticipantCourseDTO;
import spring.entities.ParticipantCourse;
import spring.entities.UserAccount;
import spring.services.ParticipantCourseService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200") // CORS policy: No 'Access-Control-Allow-Origin'
@RestController
@RequestMapping("/participantCourse") // http://localhost:8080(/participantCourse)
public class ParticipantCourseController {

    @Autowired
    ParticipantCourseService participantCourseService;

    //@GetMapping
    //@PostMapping
    //@PutMapping
    //@DeleteMapping

    //---------------- (GET Requests)

    // Find All Participant Courses
    @GetMapping(value={"allParticipantCourses"})// (/participantCourse)/allParticipantCourses
    private List<ParticipantCourse> getAllParticipantCourses(){
        return participantCourseService.findAllParticipantCourses();
    }

    // GET All Courses of User
    @GetMapping(value={"allCoursesOfUser"})
    private List<String> findAllCoursesOfUser(@RequestParam("username") String username){
        return participantCourseService.findAllCoursesOfUser(username);
    }

    // GET All Courses not Followed by User
    @GetMapping(value={"allCoursesNotFollowedByUser"})
    private List<String> findAllCoursesNotFollowedByUser(@RequestParam("username") String username){
        return participantCourseService.findAllCoursesNotFollowedByUser(username);
    }

    // GET All Participants of Course
    @GetMapping(value={"allParticipantsOfCourse"})
    private List<UserAccount> findAllParticipantsOfCourse(@RequestParam("courseName") String courseName){
        return participantCourseService.findAllParticipantsOfCourse(courseName);
    }

    // GET All Students of Course
    @GetMapping(value={"allStudentsOfCourse"})
    private List<String> findAllStudentsOfCourse(@RequestParam("courseName") String courseName){
        return participantCourseService.findAllStudentsOfCourse(courseName);
    }

    // GET All Teachers of Course
    @GetMapping(value={"allTeachersOfCourse"})
    private List<String> findAllTeachersOfCourse(@RequestParam("courseName") String courseName){
        return participantCourseService.findAllTeachersOfCourse(courseName);
    }

    // CHECK/GET User Assigned to Course
    @GetMapping(value={"checkUserAssignedToCourse"})
    private ParticipantCourse checkUserAssignedToCourse(@RequestParam("username") String username, @RequestParam("courseName") String courseName){
        return participantCourseService.checkUserAssignedToCourse(username, courseName);
    }

    //---------------- (POST Requests)

    // INSERT Participant Course
    @PostMapping(value = { "/post/insertParticipantCourse" })
    private void addParticipantCourse(@RequestBody ParticipantCourseDTO participantCourse) {
        participantCourseService.addParticipantCourse(participantCourse);
    }

    //---------------- (DELETE Requests)

    // DELETE Participant Course
    @DeleteMapping(value = { "/delete/deleteParticipantCourse" })
    private void deleteParticipantCourse(@RequestParam("username") String username, @RequestParam("courseName") String courseName) {
        participantCourseService.deleteParticipantCourse(username, courseName);
    }
}
