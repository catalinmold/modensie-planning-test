package spring.dto;

import java.util.Date;

public class MeetingDTO
{
    private String courseName;
    private String name;
    private String room;
    private Date start_time;
    private Date end_time;
    private String description;
    private String link;

    // Constructors
    public MeetingDTO() { }

    public MeetingDTO(String courseName, String name, String room, Date start_time, Date end_time, String description, String link) {
        this.courseName = courseName;
        this.name = name;
        this.room = room;
        this.start_time = start_time;
        this.end_time = end_time;
        this.description = description;
        this.link = link;
    }

    // Getters and Setters
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
