package spring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="Participant_Course")
public class ParticipantCourse {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "participant_course_id", columnDefinition = "BINARY(16)")
    private UUID participantCourseId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "course_id", nullable = false)
    @JsonIgnore
    private Course courseId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private UserAccount userId;

    // Constructors
    public ParticipantCourse() {
    }

    public ParticipantCourse(Course courseId, UserAccount userId) {
        this.courseId = courseId;
        this.userId = userId;
    }

    public ParticipantCourse(UUID participantCourseId, Course courseId, UserAccount userId) {
        this.participantCourseId = participantCourseId;
        this.courseId = courseId;
        this.userId = userId;
    }

    // Getters and Setters
    public UUID getParticipantCourseId() {
        return participantCourseId;
    }

    public void setParticipantCourseId(UUID participantCourseId) {
        this.participantCourseId = participantCourseId;
    }

    public Course getCourseId() {
        return courseId;
    }

    public void setCourseId(Course courseId) {
        this.courseId = courseId;
    }

    public UserAccount getUserId() {
        return userId;
    }

    public void setUserId(UserAccount userId) {
        this.userId = userId;
    }
}