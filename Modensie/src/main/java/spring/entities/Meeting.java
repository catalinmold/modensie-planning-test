package spring.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="Meeting")
public class Meeting {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "meeting_id", columnDefinition = "BINARY(16)")
    private UUID meetingId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "course_id", nullable = false)
    @JsonIgnore
    private Course courseId;

    @Column(name = "name")
    private String name;

    @Column(name = "room")
    private String room;

    @JsonFormat(pattern = "dd-MM-yyyy hh:mm")
    @Column(name = "start_time")
    private Date start_time;

    @JsonFormat(pattern = "dd-MM-yyyy hh:mm")
    @Column(name = "end_time")
    private Date end_time;

    @Column(name = "description", nullable = true)
    private String description;

    @Column(name = "link")
    private String link;

    // Constructors
    public Meeting() {
    }

    public Meeting(UUID meetingId, Course courseId, String name, String room, Date start_time, Date end_time, String description, String link) {
        this.meetingId = meetingId;
        this.courseId = courseId;
        this.name = name;
        this.room = room;
        this.start_time = start_time;
        this.end_time = end_time;
        this.description = description;
        this.link = link;
    }

    // Getters and Setters
    public UUID getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(UUID meetingId) {
        this.meetingId = meetingId;
    }

    public Course getCourseId() {
        return courseId;
    }

    public void setCourseId(Course courseId) {
        this.courseId = courseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}