package spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.dto.AssignmentDTO;
import spring.dto.MeetingDTO;
import spring.entities.Meeting;
import spring.repositories.CourseRepository;
import spring.repositories.MeetingRepository;

import java.util.*;

@Service
public class MeetingService {

    @Autowired
    private MeetingRepository meetingRepository;
    @Autowired
    private CourseRepository courseRepository;

    /*
    // ADD
    public void addAssignment(AssignmentDTO assignment){
        UserAccount user = userAccountRepository.findByUsername(assignment.getTeacher());
        Course course =  courseRepository.findByCourseName(assignment.getCourseName());

        Assignment newAssignment= new Assignment(course, user, assignment.getName(), assignment.getDate(), assignment.getType(), assignment.getDescription());
        assignmentRepository.save(newAssignment);
    }
    */

    // FIND
    public List<MeetingDTO> findAllMeetingsOfUser(String username) {
        List<Object[]> meetings = meetingRepository.findAllMeetingsOfUser(username);

        ArrayList<MeetingDTO> newList = new ArrayList<MeetingDTO>();
        for(Object[] meeting: meetings) {
            newList.add(new MeetingDTO(meeting[0].toString(), meeting[3].toString(), meeting[4].toString(), (Date) meeting[5], (Date) meeting[6], meeting[7].toString(), meeting[8].toString()));
        }
        return newList;
    }

    /*
    // DELETE
    public void deleteAssignment(String assignmentName, String courseName)
    {
        Assignment assignment = checkAssignmentAssignedToCourse(assignmentName, courseName);
        assignmentRepository.delete(assignment);
    }
    */
}
